# Friulian translation for boatswain.
# Copyright (C) 2023 boatswain's COPYRIGHT HOLDER
# This file is distributed under the same license as the boatswain package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: boatswain main\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/boatswain/issues\n"
"POT-Creation-Date: 2024-03-14 13:01+0000\n"
"PO-Revision-Date: 2024-04-24 17:20+0200\n"
"Last-Translator: Fabio T. <f.t.public@gmail.com>\n"
"Language-Team: Friulian <f.t.public@gmail.com>\n"
"Language: fur\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Editor: HaiPO 1.4 beta\n"
"X-Generator: Poedit 3.4.2\n"

#: data/com.feaneron.Boatswain.desktop.in.in:3
#: data/com.feaneron.Boatswain.metainfo.xml.in.in:6
msgid "Boatswain"
msgstr "Boatswain"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/com.feaneron.Boatswain.desktop.in.in:11
msgid "stream deck;streaming;deck;elgato;"
msgstr "stream deck;streaming;deck;elgato;"

#: data/com.feaneron.Boatswain.metainfo.xml.in.in:12
msgid "Control your Elgato Stream Decks"
msgstr "Controle i tiei Stream Decks di Elgato"

#: data/com.feaneron.Boatswain.metainfo.xml.in.in:14
msgid "Boatswain allows you to control Elgato Stream Deck devices."
msgstr "Boatswain ti permet di controlâ i dispositîfs Stream Deck di Elgato."

#: data/com.feaneron.Boatswain.metainfo.xml.in.in:15
msgid "With Boatswain you will be able to:"
msgstr "Cun Boatswain tu podarâs:"

#: data/com.feaneron.Boatswain.metainfo.xml.in.in:17
msgid "Organize your actions in pages and profiles"
msgstr "Organizâ lis azions in pagjinis e profîi"

#: data/com.feaneron.Boatswain.metainfo.xml.in.in:18
msgid "Set custom icons to actions"
msgstr "Configurâ iconis personalizadis a azions"

#: data/com.feaneron.Boatswain.metainfo.xml.in.in:19
msgid "Control your music player"
msgstr "Controlâ il to riprodutôr musicâl"

#: data/com.feaneron.Boatswain.metainfo.xml.in.in:20
msgid "Play sound effects during your streams"
msgstr "Riprodusi efiets sonôrs dilunc i tiei flus"

#: data/com.feaneron.Boatswain.metainfo.xml.in.in:21
msgid ""
"Control OBS Studio using Stream Deck (requires the obs-websocket extension)"
msgstr ""
"Controlâ OBS Studio doprant Stream Deck (e covente la estension obs-"
"websocket)"

#: data/com.feaneron.Boatswain.metainfo.xml.in.in:22
msgid "Send network requests"
msgstr "Invie richiestis di rêt"

#: data/com.feaneron.Boatswain.metainfo.xml.in.in:23
msgid "Keep track of your gaming score"
msgstr "Ten segnâts i ponts dal to zûc"

#: data/com.feaneron.Boatswain.metainfo.xml.in.in:24
msgid "Open files and launch applications"
msgstr "Vierç files e invie aplicazions"

#: data/com.feaneron.Boatswain.gschema.xml:7
#: data/com.feaneron.Boatswain.gschema.xml:8
msgid "Restore token for the desktop controller"
msgstr "Ripristine gjeton pal controlôr dal scritori"

#: src/bs-application.c:54
msgid "Enable debug messages"
msgstr "Abilite messaçs di debug"

#: src/bs-application.c:117
#, c-format
msgid "Connected to %d device"
msgid_plural "Connected to %d devices"
msgstr[0] "Conetût a %d dispositîf"
msgstr[1] "Conetût a %d dispositîfs"

#: src/bs-application.c:169
msgid ""
"Boatswain needs to run in background to detect and execute Stream Deck "
"actions."
msgstr ""
"Al è necessari che Boatswain al ziri in sotfont cussì di rilevâ e eseguî lis "
"azions dal Stream Deck."

#: src/bs-button-editor.c:388
msgid "All supported formats"
msgstr "Ducj i formâts supuartâts"

#: src/bs-button-editor.c:397
msgid "Select icon"
msgstr "Selezione icone"

#: src/bs-button-editor.c:398
#: src/plugins/network/network-http-action-prefs.c:147
msgid "Open"
msgstr "Vierç"

#: src/bs-button-editor.ui:14
msgid "Button Properties"
msgstr "Proprietâts dal boton"

#: src/bs-button-editor.ui:190
msgid "Name"
msgstr "Non"

#: src/bs-button-editor.ui:197
msgid "Background Color"
msgstr "Colôr di fonts"

#: src/bs-button-editor.ui:233
msgid "Select action"
msgstr "Selezione azion"

#: src/bs-button-editor.ui:254
msgid "Remove Action"
msgstr "Gjave azion"

#: src/bs-button-editor.ui:277
#: src/plugins/default/default-multi-action-editor.ui:67
msgid "Actions"
msgstr "Azions"

#: src/bs-profile.c:184
msgid "Unnamed profile"
msgstr "Profîl cence non"

#: src/bs-profile-row.ui:8 src/bs-profile-row.ui:87
msgid "Rename"
msgstr "Cambie non"

#: src/bs-profile-row.ui:14
msgid "Move up"
msgstr "Sposte in sù"

#: src/bs-profile-row.ui:18
msgid "Move down"
msgstr "Sposte in jù"

#: src/bs-profile-row.ui:24
msgid "Delete"
msgstr "Elimine"

#: src/bs-stream-deck.c:326
msgid "Default"
msgstr "Predefinît"

#. Translators: this is a product name. In most cases, it is not translated.
#. * Please verify if Elgato translates their product names on your locale.
#.
#: src/bs-stream-deck.c:996 src/bs-stream-deck.c:1021
msgid "Stream Deck Mini"
msgstr "Stream Deck Mini"

#. Translators: this is a product name. In most cases, it is not translated.
#. * Please verify if Elgato translates their product names on your locale.
#.
#: src/bs-stream-deck.c:1046 src/bs-stream-deck.c:1071
#: src/plugins/default/default-switch-profile-action.c:283
#: src/plugins/default/default.plugin.desktop.in:3
msgid "Stream Deck"
msgstr "Stream Deck"

#. Translators: this is a product name. In most cases, it is not translated.
#. * Please verify if Elgato translates their product names on your locale.
#.
#: src/bs-stream-deck.c:1096 src/bs-stream-deck.c:1121
msgid "Stream Deck XL"
msgstr "Stream Deck XL"

#. Translators: this is a product name. In most cases, it is not translated.
#. * Please verify if Elgato translates their product names on your locale.
#.
#: src/bs-stream-deck.c:1146
msgid "Stream Deck MK.2"
msgstr "Stream Deck MK.2"

#. Translators: this is a product name. In most cases, it is not translated.
#. * Please verify if Elgato translates their product names on your locale.
#.
#: src/bs-stream-deck.c:1171
msgid "Stream Deck Pedal"
msgstr "Stream Deck Pedal"

#. Translators: this is a product name. In most cases, it is not translated.
#. * Please verify if Elgato translates their product names on your locale.
#.
#: src/bs-stream-deck.c:1196
#| msgid "Stream Deck"
msgid "Stream Deck +"
msgstr "Stream Deck +"

#: src/bs-stream-deck.c:1275
msgid "Feaneron Hangar Original"
msgstr "Feaneron Hangar Original"

#: src/bs-stream-deck.c:1297
msgid "Feaneron Hangar XL"
msgstr "Feaneron Hangar XL"

#: src/bs-stream-deck-editor.ui:61
msgid "Select a button"
msgstr "Selezione un boton"

#: src/bs-window.ui:113
msgid "New profile…"
msgstr "Gnûf profîl…"

#: src/bs-window.ui:138 src/plugins/default/default-action-factory.c:54
msgid "Brightness"
msgstr "Luminositât"

#: src/bs-window.ui:174
msgid "Firmware"
msgstr "Firmware"

#: src/bs-window.ui:232
msgid "No Stream Deck Found"
msgstr "Nissun Stream Deck cjatât"

#: src/bs-window.ui:233
msgid "Plug in a Stream Deck device to use it."
msgstr "Tache un dispositîf Stream Deck par doprâlu."

#: src/bs-window.ui:256
msgid "_Keyboard Shortcuts"
msgstr "_Scurtis di tastiere"

#: src/bs-window.ui:260
msgid "_About Boatswain"
msgstr "_Informazions su Boatswain"

#: src/bs-window.ui:264
msgid "_Quit"
msgstr "_Jes"

#: src/bs-window.c:272
msgid "Repository"
msgstr "Repository"

#: src/gtk/help-overlay.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr "Gjenerâl"

#: src/gtk/help-overlay.ui:14
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "Mostre scurtis"

#: src/gtk/help-overlay.ui:20
msgctxt "shortcut window"
msgid "Quit"
msgstr "Jes"

#: src/plugins/default/default-action-factory.c:42
msgid "Folder"
msgstr "Cartele"

#: src/plugins/default/default-action-factory.c:48
msgid "Switch Profile"
msgstr "Cambie profîl"

#: src/plugins/default/default-action-factory.c:60
msgid "Multiple Actions"
msgstr "Azions multiplis"

#. Translators: "Constant" as in "Constant value of brightness"
#: src/plugins/default/default-brightness-action.c:141
msgid "Constant"
msgstr "Costant"

#: src/plugins/default/default-brightness-action.c:142
msgid "Increase"
msgstr "Incrès"

#: src/plugins/default/default-brightness-action.c:143
msgid "Decrease"
msgstr "Cale"

#: src/plugins/default/default-brightness-action.c:146
msgid "Mode"
msgstr "Modalitât"

#: src/plugins/default/default-brightness-action.c:153
msgid "Value"
msgstr "Valôr"

#: src/plugins/default/default-multi-action-editor.c:310
#: src/plugins/default/default-multi-action-row.c:143
msgid "Delay"
msgstr "Ritart"

#: src/plugins/default/default-multi-action-editor.ui:31
msgid "Add action"
msgstr "Zonte azion"

#: src/plugins/default/default-multi-action-editor.ui:49
msgid "Add Action"
msgstr "Zonte azion"

#: src/plugins/default/default-multi-action-editor.ui:73
msgid "Others"
msgstr "Altris"

#: src/plugins/default/default-multi-action-editor.ui:83
msgid "Settings"
msgstr "Impostazions"

#: src/plugins/default/default-switch-profile-action.c:306
msgid "Profile"
msgstr "Profîl"

#: src/plugins/desktop/desktop-action-factory.c:41
#: src/plugins/desktop/desktop-keyboard-shortcut-action.c:257
msgid "Keyboard Shortcut"
msgstr "Scurtis di tastiere"

#: src/plugins/desktop/desktop-action-factory.c:42
msgid "Trigger a keyboard shortcut"
msgstr "Ative une scurte di tastiere"

#: src/plugins/desktop/desktop-shortcut-dialog.ui:7
msgid "Set Shortcut"
msgstr "Stabilìs scurte"

#: src/plugins/desktop/desktop-shortcut-dialog.ui:66
msgid "Press Esc to cancel"
msgstr "Frache Esc par anulâ"

#: src/plugins/desktop/desktop-shortcut-dialog.ui:100
msgid "_Cancel"
msgstr "_Anule"

#: src/plugins/desktop/desktop-shortcut-dialog.ui:109
msgid "_Set"
msgstr "_Stabilìs"

#: src/plugins/desktop/desktop.plugin.desktop.in:3
msgid "Desktop"
msgstr "Scritori"

#: src/plugins/gaming/gaming.js:37
msgid "Score"
msgstr "Ponts"

#: src/plugins/gaming/gaming.js:38
msgid "Keep track of your score. Reset with a long press."
msgstr "Ten segnâts i tiei ponts. Azere cuntune pression sprolungjade."

#: src/plugins/gaming/gaming.plugin.desktop.in:3
msgid "Gaming"
msgstr "Zûc"

#: src/plugins/gaming/score.js:39
msgid "score.txt"
msgstr "ponts.txt"

#. Restore score
#: src/plugins/gaming/score.js:322
msgid "Restore Score"
msgstr "Ripristine ponts"

#: src/plugins/gaming/score.js:341
msgid "Text Color"
msgstr "Colôr dal test"

#: src/plugins/gaming/score.js:354
msgid "Save to File"
msgstr "Salve su file"

#: src/plugins/gaming/score.js:355
msgid ""
"Save score to a text file. Other apps such as OBS Studio may read from it."
msgstr ""
"Salve i ponts suntun file di test. Altris aplicazions come OBS Studio a "
"puedin rivâ a leiju."

#: src/plugins/gaming/score.js:364
#: src/plugins/launcher/launcher-open-file-action.c:209
#: src/plugins/network/network-http-action-prefs.ui:61
#: src/plugins/soundboard/soundboard-play-action-prefs.ui:8
msgid "File"
msgstr "File"

#: src/plugins/gaming/score.js:383
msgid "Text File (.txt)"
msgstr "File di test (.txt)"

#: src/plugins/launcher/launcher-action-factory.c:41
msgid "Launch Application"
msgstr "Invie aplicazion"

#: src/plugins/launcher/launcher-action-factory.c:47
msgid "Open File"
msgstr "Vierç file"

#: src/plugins/launcher/launcher-action-factory.c:53
msgid "Open URL"
msgstr "Vierç URL"

#: src/plugins/launcher/launcher-launch-preferences.ui:8
msgid "Choose Application"
msgstr "Sielç aplicazion"

#: src/plugins/launcher/launcher-open-file-action.c:68
msgid "No file selected"
msgstr "Nissun file selezionât"

#: src/plugins/launcher/launcher-open-file-action.c:156
msgid "Select File"
msgstr "Selezione un file"

#: src/plugins/launcher/launcher-open-url-action.c:104
msgid "URL"
msgstr "URL"

#: src/plugins/launcher/launcher.plugin.desktop.in:3
msgid "Launcher"
msgstr "Inviadôr"

#: src/plugins/network/network-action-factory.c:41
msgid "HTTP Request"
msgstr "Richieste HTTP"

#: src/plugins/network/network-action-factory.c:42
msgid "Send GET and POST requests to a webserver"
msgstr "Invie richiestis GET e POST a un servidôr web"

#: src/plugins/network/network-http-action-prefs.c:146
msgid "Select payload file"
msgstr "Selezione file di dâts utii"

#: src/plugins/network/network-http-action-prefs.ui:14
msgid "URI"
msgstr "URI"

#: src/plugins/network/network-http-action-prefs.ui:22
msgid "Method"
msgstr "Metodi"

#: src/plugins/network/network-http-action-prefs.ui:45
msgid "Payload source"
msgstr "Sorzint dâts utii"

#: src/plugins/network/network-http-action-prefs.ui:82
msgid "Edit payload"
msgstr "Modifiche dâts utii"

#: src/plugins/network/network-http-action-prefs.ui:99
msgid "HTTP Request Payload"
msgstr "Dâts utii de richieste HTTP"

#: src/plugins/network/network.plugin.desktop.in:3
msgid "Network"
msgstr "Rêt"

#: src/plugins/obs-studio/obs-action-factory.c:46
msgid "Switch Scene"
msgstr "Cambie sene"

#. Translators: "Record" is a verb here
#: src/plugins/obs-studio/obs-action-factory.c:53
msgid "Record"
msgstr "Regjistre"

#. Translators: "Stream" is a verb here
#: src/plugins/obs-studio/obs-action-factory.c:60
msgid "Stream"
msgstr "Flus"

#: src/plugins/obs-studio/obs-action-factory.c:66
msgid "Virtual Camera"
msgstr "Fotocjamare virtuâl"

#: src/plugins/obs-studio/obs-action-factory.c:72
msgid "Toggle Mute"
msgstr "Comute cidin"

#: src/plugins/obs-studio/obs-action-factory.c:78
msgid "Show / Hide Source"
msgstr "Mostre / Plate sorzint"

#: src/plugins/obs-studio/obs-connection.c:800
msgid "Invalid password"
msgstr "Password no valide"

#: src/plugins/obs-studio/obs-connection-settings.ui:8
msgid "Connection Settings"
msgstr "Impostazions di conession"

#: src/plugins/obs-studio/obs-connection-settings.ui:12
msgid "Host"
msgstr "Host"

#: src/plugins/obs-studio/obs-connection-settings.ui:20
msgid "Port"
msgstr "Puarte"

#: src/plugins/obs-studio/obs-connection-settings.ui:45
msgid "Password"
msgstr "Password"

#: src/plugins/obs-studio/obs-switch-scene-action.c:283
msgid "Scene"
msgstr "Sene"

#: src/plugins/obs-studio/obs-toggle-source-action.c:124
msgid "Toggle mute"
msgstr "Comute cidin"

#: src/plugins/obs-studio/obs-toggle-source-action.c:125
msgid "Mute"
msgstr "Cidine"

#: src/plugins/obs-studio/obs-toggle-source-action.c:126
msgid "Unmute"
msgstr "Gjave cidin"

#: src/plugins/obs-studio/obs-toggle-source-action.c:130
msgid "Toggle visibility"
msgstr "Comute visibilitât"

#: src/plugins/obs-studio/obs-toggle-source-action.c:131
msgid "Hide"
msgstr "Plate"

#: src/plugins/obs-studio/obs-toggle-source-action.c:132
msgid "Show"
msgstr "Mostre"

#: src/plugins/obs-studio/obs-toggle-source-action.c:479
#: src/plugins/soundboard/soundboard-play-action-prefs.ui:22
msgid "Behavior"
msgstr "Compuartament"

#: src/plugins/obs-studio/obs-toggle-source-action.c:490
msgid "Audio Source"
msgstr "Sorzint audio"

#: src/plugins/obs-studio/obs-toggle-source-action.c:492
msgid "Source"
msgstr "Sorzint"

#: src/plugins/obs-studio/obs-studio.plugin.desktop.in:3
msgid "OBS Studio"
msgstr "OBS Studio"

#: src/plugins/soundboard/soundboard-action-factory.c:42
msgid "Music Player"
msgstr "Letôr musicâl"

#: src/plugins/soundboard/soundboard-action-factory.c:43
msgid "Control the active music player"
msgstr "Controle la ativitât dal letôr musicâl"

#: src/plugins/soundboard/soundboard-action-factory.c:48
msgid "Play Audio"
msgstr "Riprodûs audio"

#: src/plugins/soundboard/soundboard-mpris-action.c:197
msgid "Next"
msgstr "Sucessîf"

#: src/plugins/soundboard/soundboard-mpris-action.c:198
msgid "Pause"
msgstr "Pause"

#: src/plugins/soundboard/soundboard-mpris-action.c:199
msgid "Play"
msgstr "Riprodûs"

#: src/plugins/soundboard/soundboard-mpris-action.c:200
msgid "Previous"
msgstr "Precedent"

#: src/plugins/soundboard/soundboard-mpris-action.c:201
msgid "Stop"
msgstr "Ferme"

#: src/plugins/soundboard/soundboard-mpris-action.c:202
msgid "Play / Pause"
msgstr "Riprodûs / Pause"

#: src/plugins/soundboard/soundboard-mpris-action.c:205
msgid "Playback Action"
msgstr "Azion di riproduzion"

#: src/plugins/soundboard/soundboard-play-action-prefs.c:96
msgid "Audio Files"
msgstr "Files audio"

#: src/plugins/soundboard/soundboard-play-action-prefs.c:103
msgid "Select audio file"
msgstr "Selezione file audio"

#: src/plugins/soundboard/soundboard-play-action-prefs.ui:27
msgid "Play / Stop"
msgstr "Riprodûs / Ferme"

#: src/plugins/soundboard/soundboard-play-action-prefs.ui:28
msgid "Play / Overlap"
msgstr "Riprodûs / Sorepon"

#: src/plugins/soundboard/soundboard-play-action-prefs.ui:29
msgid "Play / Restart"
msgstr "Rirodûs / Torne scomence"

#: src/plugins/soundboard/soundboard-play-action-prefs.ui:30
msgid "Loop / Stop"
msgstr "Cicli / Ferme"

#: src/plugins/soundboard/soundboard-play-action-prefs.ui:31
msgid "Press & Hold"
msgstr "Frache e ten"

#: src/plugins/soundboard/soundboard-play-action-prefs.ui:40
msgid "Volume"
msgstr "Volum"

#: src/plugins/soundboard/soundboard.plugin.desktop.in:3
msgid "Soundboard"
msgstr "Schede audio"

#~ msgid "Georges Basile Stavracas Neto"
#~ msgstr "Georges Basile Stavracas Neto"

#~ msgid "Profiles"
#~ msgstr "Profîi"

#~ msgid "Manage profiles…"
#~ msgstr "Gjestìs profîi…"

#~ msgid "Create"
#~ msgstr "Cree"

#~ msgid "Power"
#~ msgstr "Alimentazion"

#~ msgid "Device"
#~ msgstr "Dispositîf"

#~ msgid "Serial Number"
#~ msgstr "Numar seriâl"

#~ msgid "Devices"
#~ msgstr "Dispositîfs"
