# Serbian translation for boatswain.
# Copyright © 2022 boatswain's COPYRIGHT HOLDER
# This file is distributed under the same license as the boatswain package.
# Мирослав Николић <miroslavnikolic@rocketmail.com>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: boatswain main\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/boatswain/issues\n"
"POT-Creation-Date: 2022-07-27 01:02+0000\n"
"PO-Revision-Date: 2022-10-12 10:46+0200\n"
"Last-Translator: Мирослав Николић <miroslavnikolic@rocketmail.com>\n"
"Language-Team: Serbian <српски <gnome-sr@googlegroups.org>>\n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2\n"

#: data/com.feaneron.Boatswain.desktop.in.in:3
#: data/com.feaneron.Boatswain.metainfo.xml.in.in:6
msgid "Boatswain"
msgstr "Боатсвајн"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/com.feaneron.Boatswain.desktop.in.in:11
msgid "stream deck;streaming;deck;elgato;"
msgstr "ток;дек тока;дек;stream deck;streaming;deck;elgato;"

#: data/com.feaneron.Boatswain.metainfo.xml.in.in:10
#| msgid "Control your Elgato Stream Deck devices"
msgid "Control your Elgato Stream Decks"
msgstr "Управљајте вашим „Elgato Stream Deck“-овима"

#: data/com.feaneron.Boatswain.metainfo.xml.in.in:12
msgid "Boatswain allows you to control Elgato Stream Deck devices."
msgstr ""
"Боатсвајн вам омогућава да управљате вашим „Elgato Stream Deck“ уређајима."

#: data/com.feaneron.Boatswain.metainfo.xml.in.in:13
msgid "With Boatswain you will be able to:"
msgstr "Са Боатсвајном бићете у могућности да:"

#: data/com.feaneron.Boatswain.metainfo.xml.in.in:15
msgid "Organize your actions in pages and profiles"
msgstr "Организујете ваше радње у страницама и профилима"

#: data/com.feaneron.Boatswain.metainfo.xml.in.in:16
msgid "Set custom icons to actions"
msgstr "Поставите произвољне иконице за радње"

#: data/com.feaneron.Boatswain.metainfo.xml.in.in:17
msgid "Control your music player"
msgstr "Управљате вашим програмом за пуштање музике"

#: data/com.feaneron.Boatswain.metainfo.xml.in.in:18
msgid "Play sound effects during your streams"
msgstr "Пуштате звучна дејства за време ваших стримовања"

#: data/com.feaneron.Boatswain.metainfo.xml.in.in:19
msgid ""
"Control OBS Studio using Stream Deck (requires the obs-websocket extension)"
msgstr ""
"Управљате OBS Студиом користећи „Stream Deck“ (захтева проширење „obs-"
"websocket“)"

#. Translators: please do NOT translate this
#: data/com.feaneron.Boatswain.metainfo.xml.in.in:23
msgid "Georges Basile Stavracas Neto"
msgstr "Џорџ Базиле Ставракас Нето"

#: src/bs-application.c:52
msgid "Enable debug messages"
msgstr "Укључи поруке прочишћавања"

#: src/bs-application.c:128
msgid ""
"Boatswain needs to run in background to detect and execute Stream Deck "
"actions."
msgstr ""
"Боатсвајн треба да ради у позадини да би открио и извршио радње „Stream "
"Deck“-а."

#: src/bs-profile.c:184
msgid "Unnamed profile"
msgstr "Неименовани профил"

#: src/bs-profile-row.ui:8 src/bs-profile-row.ui:58
msgid "Rename"
msgstr "Преименуј"

#: src/bs-profile-row.ui:14
msgid "Move _up"
msgstr "Помери _горе"

#: src/bs-profile-row.ui:18
msgid "Move _down"
msgstr "Помери _доле"

#: src/bs-profiles-dialog.ui:26
msgid "Profiles"
msgstr "Профили"

#: src/bs-stream-deck.c:277
msgid "Default"
msgstr "Основно"

#. Translators: this is a product name. In most cases, it is not translated.
#. * Please verify if Elgato translates their product names on your locale.
#.
#: src/bs-stream-deck.c:820
msgid "Stream Deck Mini"
msgstr "Stream Deck Mini"

#. Translators: this is a product name. In most cases, it is not translated.
#. * Please verify if Elgato translates their product names on your locale.
#.
#: src/bs-stream-deck.c:846 src/bs-stream-deck.c:872
#: src/plugins/default/default-switch-profile-action.c:285
msgid "Stream Deck"
msgstr "Stream Deck"

#. Translators: this is a product name. In most cases, it is not translated.
#. * Please verify if Elgato translates their product names on your locale.
#.
#: src/bs-stream-deck.c:898
msgid "Stream Deck XL"
msgstr "Stream Deck XL"

#. Translators: this is a product name. In most cases, it is not translated.
#. * Please verify if Elgato translates their product names on your locale.
#.
#: src/bs-stream-deck.c:924
msgid "Stream Deck MK.2"
msgstr "Stream Deck MK.2"

#: src/bs-stream-deck.c:993
msgid "Feaneron Hangar Original"
msgstr "Feaneron Hangar Original"

#: src/bs-stream-deck.c:1016
msgid "Feaneron Hangar XL"
msgstr "Feaneron Hangar XL"

#: src/bs-stream-deck-button-editor.c:380
msgid "Select icon"
msgstr "Изаберите иконицу"

#: src/bs-stream-deck-button-editor.c:383
#: src/plugins/soundboard/soundboard-play-action-prefs.c:98
msgid "_Open"
msgstr "_Отвори"

#: src/bs-stream-deck-button-editor.c:384
#: src/plugins/soundboard/soundboard-play-action-prefs.c:99
msgid "_Cancel"
msgstr "_Откажи"

#: src/bs-stream-deck-button-editor.c:388
msgid "All supported formats"
msgstr "Сви подржани формати"

#: src/bs-stream-deck-button-editor.ui:17
msgid "Select a button"
msgstr "Изаберите дугме"

#: src/bs-stream-deck-button-editor.ui:188
msgid "Name"
msgstr "Назив"

#: src/bs-stream-deck-button-editor.ui:195
msgid "Background Color"
msgstr "Боја позадине"

#: src/bs-stream-deck-button-editor.ui:226
#: src/bs-stream-deck-button-editor.ui:288
msgid "Select action"
msgstr "Изаберите радњу"

#: src/bs-stream-deck-button-editor.ui:247
msgid "Remove Action"
msgstr "Уклони радњу"

#: src/bs-window.ui:13
msgid "_New profile"
msgstr "_Нови профил"

#: src/bs-window.ui:19
msgid "Manage profiles…"
msgstr "Управљајте профилима…"

#: src/bs-window.ui:61
msgid "Create"
msgstr "Направи"

#: src/bs-window.ui:105
msgid "Power"
msgstr "Снага"

#: src/bs-window.ui:121 src/plugins/default/default-action-factory.c:56
msgid "Brightness"
msgstr "Осветљеност"

#: src/bs-window.ui:157
msgid "Device"
msgstr "Уређај"

#: src/bs-window.ui:173
msgid "Serial Number"
msgstr "Серијски број"

#: src/bs-window.ui:200
msgid "Firmware"
msgstr "Уграђени програм"

#: src/bs-window.ui:248
msgid "Devices"
msgstr "Уређаји"

#: src/bs-window.ui:292
msgid "No Stream Deck Found"
msgstr "Нисам нашао „Stream Deck“"

#: src/bs-window.ui:293
msgid "Plug in a Stream Deck device to use it."
msgstr "Прикључите „Stream Deck“ уређај да бисте га користили."

#: src/bs-window.ui:304
msgid "_Keyboard Shortcuts"
msgstr "_Пречице тастатуре"

#: src/bs-window.ui:308
msgid "_About Boatswain"
msgstr "_О Боатсвајну"

#: src/bs-window.ui:312
msgid "_Quit"
msgstr "_Изађи"

#: src/bs-window.c:329
msgid "Repository"
msgstr "Ризница"

#: src/gtk/help-overlay.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr "Опште"

#: src/gtk/help-overlay.ui:14
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "Прикажи пречице"

#: src/gtk/help-overlay.ui:20
msgctxt "shortcut window"
msgid "Quit"
msgstr "Изађи"

#: src/plugins/default/default-action-factory.c:44
msgid "Folder"
msgstr "Фасцикла"

#: src/plugins/default/default-action-factory.c:50
msgid "Switch Profile"
msgstr "Промени профил"

#: src/plugins/default/default-action-factory.c:62
msgid "Multiple Actions"
msgstr "Више радњи"

#: src/plugins/default/default-brightness-action.c:140
msgid "Constant"
msgstr "Константно"

#: src/plugins/default/default-brightness-action.c:141
msgid "Increase"
msgstr "Повећај"

#: src/plugins/default/default-brightness-action.c:142
msgid "Decrease"
msgstr "Смањи"

#: src/plugins/default/default-brightness-action.c:145
msgid "Mode"
msgstr "Режим"

#: src/plugins/default/default-brightness-action.c:152
msgid "Value"
msgstr "Вредност"

#: src/plugins/default/default-multi-action-editor.c:310
#: src/plugins/default/default-multi-action-row.c:142
msgid "Delay"
msgstr "Застој"

#: src/plugins/default/default-multi-action-editor.ui:31
msgid "Add action"
msgstr "Додајте радњу"

#: src/plugins/default/default-multi-action-editor.ui:49
msgid "Add Action"
msgstr "Додај радњу"

#: src/plugins/default/default-multi-action-editor.ui:67
msgid "Actions"
msgstr "Радње"

#: src/plugins/default/default-multi-action-editor.ui:73
msgid "Others"
msgstr "Друго"

#: src/plugins/default/default-multi-action-editor.ui:83
msgid "Settings"
msgstr "Поставке"

#: src/plugins/default/default-switch-profile-action.c:308
msgid "Profile"
msgstr "Профил"

#: src/plugins/launcher/launcher-action-factory.c:42
msgid "Launch Application"
msgstr "Покрени програм"

#: src/plugins/launcher/launcher-action-factory.c:48
msgid "Open URL"
msgstr "Отвори адресу"

#: src/plugins/launcher/launcher-launch-preferences.ui:8
msgid "Choose Application"
msgstr "Изабери програм"

#: src/plugins/launcher/launcher-open-url-action.c:101
msgid "URL"
msgstr "Адреса"

#: src/plugins/obs-studio/obs-action-factory.c:48
msgid "Switch Scene"
msgstr "Промени сцену"

#: src/plugins/obs-studio/obs-action-factory.c:54
msgid "Record"
msgstr "Сними"

#: src/plugins/obs-studio/obs-action-factory.c:60
msgid "Stream"
msgstr "Стримуј"

#: src/plugins/obs-studio/obs-action-factory.c:66
msgid "Virtual Camera"
msgstr "Виртуелна камера"

#: src/plugins/obs-studio/obs-action-factory.c:72
msgid "Toggle Mute"
msgstr "Окини утишање"

#: src/plugins/obs-studio/obs-action-factory.c:78
msgid "Show / Hide Source"
msgstr "Прикажи / сакриј извор"

#: src/plugins/obs-studio/obs-connection.c:800
msgid "Invalid password"
msgstr "Неисправна лозинка"

#: src/plugins/obs-studio/obs-connection-settings.ui:8
msgid "Connection Settings"
msgstr "Поставке повезивања"

#: src/plugins/obs-studio/obs-connection-settings.ui:12
msgid "Host"
msgstr "Домаћин"

#: src/plugins/obs-studio/obs-connection-settings.ui:20
msgid "Port"
msgstr "Прикључник"

#: src/plugins/obs-studio/obs-connection-settings.ui:45
msgid "Password"
msgstr "Лозинка"

#: src/plugins/obs-studio/obs-switch-scene-action.c:283
msgid "Scene"
msgstr "Сцена"

#: src/plugins/obs-studio/obs-toggle-source-action.c:124
msgid "Toggle mute"
msgstr "Окините утишање"

#: src/plugins/obs-studio/obs-toggle-source-action.c:125
msgid "Mute"
msgstr "Утишај"

#: src/plugins/obs-studio/obs-toggle-source-action.c:126
msgid "Unmute"
msgstr "Поништи утишање"

#: src/plugins/obs-studio/obs-toggle-source-action.c:130
msgid "Toggle visibility"
msgstr "Окините видљивост"

#: src/plugins/obs-studio/obs-toggle-source-action.c:131
msgid "Hide"
msgstr "Сакриј"

#: src/plugins/obs-studio/obs-toggle-source-action.c:132
msgid "Show"
msgstr "Прикажи"

#: src/plugins/obs-studio/obs-toggle-source-action.c:479
#: src/plugins/soundboard/soundboard-play-action-prefs.ui:22
msgid "Behavior"
msgstr "Понашање"

#: src/plugins/obs-studio/obs-toggle-source-action.c:490
msgid "Audio Source"
msgstr "Извор звука"

#: src/plugins/obs-studio/obs-toggle-source-action.c:492
msgid "Source"
msgstr "Извор"

#: src/plugins/soundboard/soundboard-action-factory.c:45
msgid "Music Player"
msgstr "Програм за пуштање музике"

#: src/plugins/soundboard/soundboard-action-factory.c:46
msgid "Control the active music player"
msgstr "Управљајте радним програмом за пуштање музике"

#: src/plugins/soundboard/soundboard-action-factory.c:51
msgid "Play Audio"
msgstr "Пусти звук"

#: src/plugins/soundboard/soundboard-mpris-action.c:196
msgid "Next"
msgstr "Следеће"

#: src/plugins/soundboard/soundboard-mpris-action.c:197
msgid "Pause"
msgstr "Застани"

#: src/plugins/soundboard/soundboard-mpris-action.c:198
msgid "Play"
msgstr "Пусти"

#: src/plugins/soundboard/soundboard-mpris-action.c:199
msgid "Previous"
msgstr "Претходно"

#: src/plugins/soundboard/soundboard-mpris-action.c:200
msgid "Stop"
msgstr "Стани"

#: src/plugins/soundboard/soundboard-mpris-action.c:201
msgid "Play / Pause"
msgstr "Пусти / Застани"

#: src/plugins/soundboard/soundboard-mpris-action.c:204
msgid "Playback Action"
msgstr "Радња пуштања"

#: src/plugins/soundboard/soundboard-play-action-prefs.c:95
msgid "Select audio file"
msgstr "Изаберите датотеку звука"

#: src/plugins/soundboard/soundboard-play-action-prefs.c:103
msgid "Audio Files"
msgstr "Датотеке звука"

#: src/plugins/soundboard/soundboard-play-action-prefs.ui:8
msgid "File"
msgstr "Датотека"

#: src/plugins/soundboard/soundboard-play-action-prefs.ui:27
msgid "Play / Stop"
msgstr "Пусти / Заустави"

#: src/plugins/soundboard/soundboard-play-action-prefs.ui:28
msgid "Play / Overlap"
msgstr "Пусти / Преклопи"

#: src/plugins/soundboard/soundboard-play-action-prefs.ui:29
msgid "Play / Restart"
msgstr "Пусти / Поново покрени"

#: src/plugins/soundboard/soundboard-play-action-prefs.ui:30
msgid "Loop / Stop"
msgstr "Понављај / Заустави"

#: src/plugins/soundboard/soundboard-play-action-prefs.ui:31
msgid "Press & Hold"
msgstr "Притисни и држи"

#: src/plugins/soundboard/soundboard-play-action-prefs.ui:40
msgid "Volume"
msgstr "Гласност"

#~ msgid "Copyright © 2022 Georges Basile Stavracas Neto"
#~ msgstr "Ауторска права © 2022 Џорџ Базиле Ставракас Нето"
